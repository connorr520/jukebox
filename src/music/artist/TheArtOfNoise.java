package music.artist;

import snhu.jukebox.playlist.Song;

import java.util.ArrayList;

public class TheArtOfNoise {
	
	ArrayList<Song> albumTracks;
	String albumTitle;
	
	public TheArtOfNoise() {
	}
	
	// New Artist The Art Of Noise
	public ArrayList<Song> getTheArtOfNoiseSongs() {
		
		albumTracks = new ArrayList<Song>();
		
		Song track1 = new Song("Close (To The Edit)" ,"The Art Of Noise");
		Song track2 = new Song("Moments In Love","The Art Of Noise");
		Song track3 = new Song("Beat Box (Diversion 1)","The Art Of Noise");
		this.albumTracks.add(track1);
		this.albumTracks.add(track2);
		this.albumTracks.add(track3);
		
		return albumTracks;
	}
	
}
