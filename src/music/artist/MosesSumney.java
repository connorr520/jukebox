package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class MosesSumney {
	
	ArrayList<Song> albumTracks;
	String albumTitle;
	
	public MosesSumney() {
	}
	
	// New Artist Moses Sumney
	public ArrayList<Song> getMosesSumneySongs() {
		
		albumTracks = new ArrayList<Song>();
		
		Song track1 = new Song("Colouor","Moses Sumney");
		Song track2 = new Song("Polly","Moses Sumney");
		Song track3 = new Song("Cut Me","Moses Sumney");
		this.albumTracks.add(track1);
		this.albumTracks.add(track2);
		this.albumTracks.add(track3);
		
		
		return albumTracks;
	}
	
}
